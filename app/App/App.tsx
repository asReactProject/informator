import * as React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.less";
import { LoginForm } from "../Containers/Loginform/LoginForm";
import { Actions } from "../Actions/Actions";
import { EmployeesPage } from "../Containers/Managers/EmployeesPage";
import { OrganizationPage } from "../Containers/Organizations/OrganizationsPage";
import { DivisionsPage } from "../Containers/Divisions/DivisionsPage";
import { Mess } from "../Components/Message";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { IStoreState, IStateProps, IDispatch } from "../Interfaces";
import { IActionType } from "../common";

import { SignInPage } from "../Containers/SignInPage/SignInPage";

/**
 * Основной класс приложения.
 */
type TProps = IStateProps & IDispatch;
class App extends React.Component<TProps, {}> {
  render() {
    const { loginStatus, currentLogin } = this.props;
    return (
      <Router>
        <div className="Main">
          <Mess loginStatus={loginStatus} currentLogin={currentLogin} />
          <SignInPage />

          <Switch>
            <Route exact path="/" component={LoginForm} />
            <Route path="/login" component={LoginForm} />
            <Route path="/organizations" component={OrganizationPage} />

            <Route exact path="/division" component={DivisionsPage} />
            <Route path="/division/:id" component={DivisionsPage} />

            <Route exact path="/employee" component={EmployeesPage} />
            <Route path="/employee/:id" component={EmployeesPage} />
          </Switch>
        </div>
      </Router>
    );
  }
}

const mapStateToProps = (store: IStoreState): IStateProps => {
  return {
    loginStatus: store.loginStatus,

    currentLogin: store.currentLogin
  };
};
const mapDispatchToProps = (dispatch: Dispatch<IActionType>): IDispatch => {
  return {
    actions: new Actions(dispatch)
  };
};

const connectApp = connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

export { connectApp as App };
