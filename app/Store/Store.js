import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import logger from "redux-logger";
import thunk from "redux-thunk";
import { reducer } from "../Reducers/Reducers";

const store = createStore(
  reducer,
  composeWithDevTools(applyMiddleware(thunk, logger))
);

export { store as Store };
