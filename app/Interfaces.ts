import { Actions } from "./Actions/Actions";

export interface IDispatch {
  actions?: Actions;
}
export interface IStoreState {
  loginStatus: boolean;
  loading: boolean;
  currentLogin: string;
  divisions: IDivisionData[];
  organizations: IDataOrganization[];
  employees: IEmployeeData[];
  isOpenOrg: boolean;
  isOpenEditOrg: false;
  organization: any;
}

export interface IDivisionData {
  id: number;
  id_organization: number;
  name: string;
  phone: number;
}

export interface IEmployeeData {
  id: number;
  id_division: number;
  FIO: string;
  address: string;
  position: string;
}

export interface IDataOrganization {
  id: number;
  name: string;
  address: string;
  INN: number;
}

interface IMatch {
  params: { id: string };
}

export interface IPropsMatch {
  match: IMatch;
}
export interface IStateProps {
  loginStatus?: boolean;
  currentLogin?: string;
}
export interface IStatePropsDivision {
  divisions: IDivisionData[];
}

export interface IStatePropsOrganization {
  organizations: IDataOrganization[];
}

export interface IStatePropsemployee {
  employees: IEmployeeData[];
}

export interface IStatePropsModalAddOrganization {
  isOpenOrg: boolean;
  organization: any;
}
export interface IStatePropsModalEditOrganization {
  isOpenEditOrg: boolean;
  organization: any;
}
