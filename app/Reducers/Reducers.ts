import { IActionType } from "../common";
import { ActionTypes, AsyncActionTypes } from "../Actions/Constants";
import { IStoreState } from "../Interfaces";

const initialState = {
  get state(): IStoreState {
    return {
      loginStatus: false,
      loading: false,
      currentLogin: "",
      organizations: [],
      divisions: [],
      employees: [],
      isOpenOrg: false,
      isOpenEditOrg: false,
      organization: {}
    };
  }
};
export function reducer(
  state: IStoreState = initialState.state,
  action: IActionType
) {
  switch (action.type) {
    case `${ActionTypes.LOGIN}${AsyncActionTypes.BEGIN}`:
      return {
        ...state,
        loading: true
      };
    case `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`:
      return {
        ...state,
        loading: false,
        currentLogin: action.payload,
        loginStatus: true
      };
    case ActionTypes.LOGOUT:
      return {
        ...state,
        loading: false,
        currentLogin: "",
        loginStatus: false
      };
    case `${ActionTypes.ORGANIZATIONS}${AsyncActionTypes.SUCCESS}`:
      return {
        ...state,
        organizations: action.organizations
      };
    case `${ActionTypes.DIVISIONS}${AsyncActionTypes.SUCCESS}`:
      return {
        ...state,
        divisions: action.divisions
      };
    case `${ActionTypes.EMPLOYEES}${AsyncActionTypes.SUCCESS}`:
      return {
        ...state,
        employees: action.employees
      };
    case `${ActionTypes.OPEN_MODAL_ADD_ORGANIZATION}`:
      return {
        ...state,
        isOpenOrg: true,
        organization: action.payload
      };
    case `${ActionTypes.CLOSE_MODAL_ADD_ORGANIZATION}`:
      return {
        ...state,
        isOpenOrg: false,
        organization: state.organization
      };
    case `${ActionTypes.OPEN_MODAL_EDIT_ORGANIZATION}`:
      return {
        ...state,
        isOpenEditOrg: true,
        organization: action.payload
      };
    case `${ActionTypes.CLOSE_MODAL_EDIT_ORGANIZATION}`:
      return {
        ...state,
        isOpenEditOrg: false,
        organization: state.organization
      };
  }
  return state;
}
