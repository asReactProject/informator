import * as React from "react";
import { IStateProps } from "../Interfaces";

type TProps = IStateProps;
export class Mess extends React.Component<TProps, {}> {
  render() {
    const { loginStatus, currentLogin } = this.props;
    return (
      <div className="LoginMessage">
        {/*отрисовку по условию */}
        {loginStatus ? <p>Привет {currentLogin}</p> : <p>Авторизируйтесь</p>}
      </div>
    );
  }
}
