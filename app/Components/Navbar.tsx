import * as React from "react";
import { NavLink } from "react-router-dom";
import { IStateProps, IDispatch } from "../Interfaces";

type TProps = IStateProps & IDispatch;
export class Navbar extends React.Component<TProps, {}> {
  render() {
    const { loginStatus } = this.props;
    return (
      <div className="MainNavBar">
        {loginStatus ? (
          <div className="Navigation">
            <div className="NavLink">
              <NavLink to="/organizations">Организации</NavLink>
            </div>
            <div className="NavLink">
              <NavLink to="/division">Подразделения</NavLink>
            </div>
            <div className="NavLink">
              <NavLink to="/employee">Работники</NavLink>
            </div>
          </div>
        ) : (
          <p>""</p>
        )}
      </div>
    );
  }
}
