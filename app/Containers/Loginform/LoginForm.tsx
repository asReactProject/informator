import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { Actions } from "../../Actions/Actions";
import { Redirect } from "react-router-dom";
import "./LoginForm.less";
import { IStoreState, IStateProps, IDispatch } from "../../Interfaces";
import { IActionType } from "../../common";

type TProps = IStateProps & IDispatch;
class LoginForm extends React.Component<TProps, {}> {
  state = {
    name: "",
    password: ""
  };

  render() {
    const { loginStatus } = this.props;

    return (
      <div>
        {loginStatus ? (
          <Redirect from="/" to="/organizations" />
        ) : (
          <div className="authorizeForm">
            <div className="authorize">
              <label>Имя:</label>
              <input
                id="login"
                type="name"
                name="name"
                onChange={(e: any) => this.setState({ name: e.target.value })}
              />
            </div>
            <div className="authorize">
              <label>Пароль:</label>
              <input
                id="password"
                type="password"
                name="password"
                onChange={(e: any) =>
                  this.setState({ password: e.target.value })
                }
              />
            </div>
            <input
              className="btn btn-outline-primary"
              type="button"
              value="login"
              onClick={this.handleLogin}
            />
          </div>
        )}
      </div>
    );
  }
  /**
   * Обработчик авторизации пользователя.
   */
  handleLogin = () => {
    let loginData = {
      name: this.state.name,
      password: this.state.password
    };
    this.props.actions.onLogin(loginData);
  };
}

const mapStateToProps = (store: IStoreState): IStateProps => {
  return {
    loginStatus: store.loginStatus,
    currentLogin: store.currentLogin
  };
};
const mapDispatchToProps = (dispatch: Dispatch<IActionType>): IDispatch => {
  return {
    actions: new Actions(dispatch)
  };
};

const connectLoginForm = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginForm);

export { connectLoginForm as LoginForm };
