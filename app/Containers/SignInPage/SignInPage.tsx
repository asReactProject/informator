import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";

import { Actions } from "../../Actions/Actions";
import { Redirect } from "react-router-dom";
import { Navbar } from "../../Components/Navbar";

import { IStoreState, IStateProps, IDispatch } from "../../Interfaces";
import { IActionType } from "../../common";

type TProps = IStateProps & IDispatch;

class SignInPage extends React.Component<TProps, {}> {
  render() {
    const { loginStatus } = this.props;
    return (
      <div className="SignInMain">
        {loginStatus ? (
          <div>
            <Navbar loginStatus={loginStatus} />
            <input
              className="btn btn-outline-warning"
              type="button"
              value="logout"
              onClick={this.handleLogout}
            />
          </div>
        ) : (
          <Redirect to="/login" />
        )}
      </div>
    );
  }
  /**
   * Обработчик выхода из системы.
   */
  handleLogout = () => {
    this.props.actions.onLogout();
  };
}

const mapStateToProps = (store: IStoreState): IStateProps => {
  return {
    loginStatus: store.loginStatus,
    currentLogin: store.currentLogin
  };
};
const mapDispatchToProps = (dispatch: Dispatch<IActionType>): IDispatch => {
  return {
    actions: new Actions(dispatch)
  };
};

const connectSignIn = connect(
  mapStateToProps,
  mapDispatchToProps
)(SignInPage);

export { connectSignIn as SignInPage };
