import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { Actions } from "../../Actions/Actions";
import { ModalEditOrganization } from "../../Containers/Modals/OrgModal/Edit/EditOrg";
import { ModalAddOrganization } from "../../Containers/Modals/OrgModal/Add/AddOrg";
import {
  IStoreState,
  IDataOrganization,
  IDispatch,
  IStatePropsOrganization
} from "../../Interfaces";
import { Link } from "react-router-dom";

import { IActionType } from "../../common";

type IOrg = IStatePropsOrganization & IDispatch & any;

class OrganizationPage extends React.Component<IOrg> {
  componentDidMount() {
    this.props.actions.OrgFethData(`http://127.0.0.1:3002/organization`);
  }

  openModalEditOrganization = (
    e: any,
    id: number,
    name: string,
    address: string,
    INN: number
  ) => {
    e.preventDefault();
    this.props.actions.openModalEditOrganization(id, name, address, INN);
  };

  // openModalAddOrganization = (e: any, id: number, name: string, address: string, INN: number)  =>  {
  //     e.preventDefault();
  //     this.props.actions.openModalAddOrganization(id, name, address, INN);
  // }

  render() {
    const organizations = this.props.organizations.map(
      (organization: IDataOrganization) => {
        const { id, name, address, INN } = organization;
        return (
          <div className="d-tr" key={id}>
            <ModalEditOrganization />
            <ModalAddOrganization />
            <div className="d-td">{id}</div>
            <div className="d-td">{name}</div>
            <div className="d-td">{address}</div>
            <div className="d-td">{INN}</div>
            <div className="d-td">
              <div className="b-container">
                <button
                  onClick={(e: any) =>
                    this.openModalEditOrganization(e, id, name, address, INN)
                  }
                >
                  Изменить
                </button>
              </div>
            </div>
            <div className="d-td">
              <div className="b-container">
                <button>
                  <Link className="link" to={`/division/${id}`}>
                    Подразделения
                  </Link>
                </button>
              </div>
            </div>
          </div>
        );
      }
    );
    return (
      <div className="d-container">
        <h1>Организации</h1>
        <div>
          <button
            className="addItem"
            onClick={this.props.actions.openModalAddOrganization}
          >
            Добавить запись
          </button>
        </div>
        <div className="d-table">
          {organizations.length > 0 ? (
            <>{organizations}</>
          ) : (
            <h1>Список пуст</h1>
          )}
        </div>
      </div>
    );
  }
}
const mapStateToProps = (store: IStoreState): IStatePropsOrganization => {
  return {
    organizations: store.organizations
  };
};
const mapDispatchToProps = (dispatch: Dispatch<IActionType>): IDispatch => {
  return {
    actions: new Actions(dispatch)
  };
};

const connectOrgPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(OrganizationPage);

export { connectOrgPage as OrganizationPage };
