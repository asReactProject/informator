import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { OrgFethData } from "../../Actions/Actions";
import { Link } from "react-router-dom";

class OrgList extends React.Component {
  state = {
    visible: false
  };
  handleReadMoreClck = e => {
    e.preventDefault();
    this.setState({ visible: true });
  };
  render() {
    const { name, address, INN, id } = this.props.data;
    const { visible } = this.state;
    return (
      <div className="organizations">
        <h3>Название: {name}</h3>
        {!visible && (
          <a onClick={this.handleReadMoreClck} href="#" className="readmore">
            Подробнее
          </a>
        )}
        {visible && (
          <div>
            <p className="org_adress">Адрес: {address}</p>
            <p className="org_INN">Инн: {INN}</p>
            <Link to={`/division/${id}`}>Подразделения компании {name}</Link>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = store => {
  return {
    organizations: store.organizations
  };
};
const mapDispatchToProps = dispatch => {
  return {
    fetchData: url => dispatch(OrgFethData(url))
  };
};

const connectArtPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(OrgList);

export { connectArtPage as OrgList };
