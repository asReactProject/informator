import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { Actions } from "../../Actions/Actions";
import { Link } from "react-router-dom";
import {
  IStoreState,
  IDivisionData,
  IDispatch,
  IPropsMatch,
  IStatePropsDivisionData
} from "../../Interfaces";
import { IActionType } from "../../common";

class DivData extends React.Component {
  state = {
    visible: false,
    id: ""
  };
  handleReadMoreClck = e => {
    e.preventDefault();
    this.setState({ visible: true });
  };
  render() {
    const { id, name, phone } = this.props.data;
    const { visible } = this.state;
    return (
      <div className="divisions">
        <h3>Название: {name}</h3>
        {!visible && (
          <a onClick={this.handleReadMoreClck} href="#" className="readmore">
            Подробнее
          </a>
        )}
        {visible && (
          <div>
            <p className="phone">Телефон: {phone}</p>
            <p className="id_division">ID: {id}</p>
            <Link to={`/employee/${id}`}>Работники подразделения {name}</Link>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = store => {
  return {
    divisions: store.divisions
  };
};
const mapDispatchToProps = dispatch => {
  return {
    actions: new Actions(dispatch)
  };
};

const connectArtPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(DivData);

export { connectArtPage as DivData };
