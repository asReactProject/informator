import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { Actions } from "../../Actions/Actions";
import { Link } from "react-router-dom";
import { DivData } from "./DivData";
import {
  IStoreState,
  IDivisionData,
  IDispatch,
  IPropsMatch,
  IStatePropsDivision
} from "../../Interfaces";
import { IActionType } from "../../common";

type IDiv = IStatePropsDivision & IDispatch & IPropsMatch;

class DivisionsPage extends React.Component<IDiv> {
  componentDidMount() {
    const id = this.props.match.params.id;
    this.props.actions.DivFethData(`http://localhost:3002/division?id=${id}`);
    console.log(id);
  }

  renderDivisions = () => {
    const { divisions } = this.props;
    let newsTemplate = null;

    if (divisions.length) {
      newsTemplate = divisions.map(function(item: IDivisionData) {
        return <DivData key={item.id} data={item} />;
      });
    } else {
      newsTemplate = <p>К сожалению подразделений нет</p>;
    }

    return newsTemplate;
  };
  render() {
    const { divisions } = this.props;

    return (
      <div className="news">
        {this.renderDivisions()}
        {divisions.length ? (
          <div>
            <strong className={"divis__count"}>
              Всего подразделений: {divisions.length}
            </strong>
          </div>
        ) : null}

        <Link to="/organizations">Назад</Link>
      </div>
    );
  }
}

const mapStateToProps = (store: IStoreState): IStatePropsDivision => {
  return {
    divisions: store.divisions
  };
};
const mapDispatchToProps = (dispatch: Dispatch<IActionType>): IDispatch => {
  return {
    actions: new Actions(dispatch)
  };
};

const connectDivPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(DivisionsPage);

export { connectDivPage as DivisionsPage };
