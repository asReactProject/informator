import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { DivFethData } from "../../Actions/Actions";
import { NavLink, Link } from "react-router-dom";

class EpmData extends React.Component {
  state = {
    visible: false,
    id: ""
  };
  handleReadMoreClck = e => {
    e.preventDefault();
    this.setState({ visible: true });
  };
  render() {
    const { FIO, address, position, id_division, id } = this.props.data;
    const { visible } = this.state;
    return (
      <div className="epmloee">
        <h3>Имя: {FIO}</h3>
        {!visible && (
          <a onClick={this.handleReadMoreClck} href="#" className="readmore">
            Подробнее
          </a>
        )}
        {visible && (
          <div>
            <p className="phone">Адресс: {address}</p>
            <p className="id_division">Должность: {position}</p>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = store => {
  return {
    emploees: store.employee
  };
};
const mapDispatchToProps = dispatch => {
  return {
    fetchData: url => dispatch(EpmFethData(url))
  };
};

const connectArtPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(EpmData);

export { connectArtPage as EmpData };
