import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { Actions } from "../../Actions/Actions";
import { Link } from "react-router-dom";
import { EmpData } from "./EmpData";

import {
  IStoreState,
  IEmployeeData,
  IDispatch,
  IPropsMatch,
  IStatePropsemployee
} from "../../Interfaces";
import { IActionType } from "../../common";

type IEmp = IStatePropsemployee & IDispatch & IPropsMatch;

class EmployeesPage extends React.Component<IEmp> {
  componentDidMount() {
    const id = this.props.match.params.id;
    this.props.actions.EpmlFethData(`http://127.0.0.1:3002/employee?id=${id}`);
    console.log(id);
  }

  renderEmploees = () => {
    const { employees } = this.props;
    let empTemplate = null;

    if (employees.length) {
      empTemplate = employees.map(function(item: IEmployeeData) {
        return <EmpData key={item.id} data={item} />;
      });
    } else {
      empTemplate = <p>К сожалению работников нет</p>;
    }

    return empTemplate;
  };
  render() {
    const { employees } = this.props;

    return (
      <div className="info">
        {this.renderEmploees()}
        {employees.length ? (
          <strong className={"emp__count"}>
            Всего раотников: {employees.length}
          </strong>
        ) : null}
        <Link to="/divisions">Назад</Link>
      </div>
    );
  }
}

const mapStateToProps = (store: IStoreState): IStatePropsemployee => {
  return {
    employees: store.employees
  };
};
const mapDispatchToProps = (dispatch: Dispatch<IActionType>): IDispatch => {
  return {
    actions: new Actions(dispatch)
  };
};

const connectEmpPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(EmployeesPage);

export { connectEmpPage as EmployeesPage };
