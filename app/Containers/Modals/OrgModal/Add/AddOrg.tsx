import * as React from "react";
import { Actions } from "../../../../Actions/Actions";
import { IStoreState } from "../../../../Interfaces";
import { IOrganization } from "../../../../Actions/Models";
import { Dispatch } from "redux";
import { IActionType } from "../../../../common";
import {
  IDispatch,
  IStatePropsModalAddOrganization
} from "../../../../Interfaces";
import { connect } from "react-redux";
import "./AddOrg.less";

type IModalEdit = IStatePropsModalAddOrganization & IDispatch;

class ModalAddOrganization extends React.Component<IModalEdit> {
  state = {
    id: 100,
    name: "",
    address: "",
    INN: 0
  };

  idChange = (e: any) => {
    this.setState({
      id: e.target.value
    });
  };

  nameChange = (e: any) => {
    this.setState({
      name: e.target.value
    });
  };

  addressChange = (e: any) => {
    this.setState({
      address: e.target.value
    });
  };

  innChange = (e: any) => {
    this.setState({
      INN: e.target.value
    });
  };

  AddOrganization = (data: IOrganization) => {
    this.props.actions.AddOrganization(data);
  };

  render() {
    const { isOpenOrg } = this.props;

    if (!isOpenOrg) return null;
    return (
      <div className="modal">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Добавление записи</h5>
              <button
                onClick={this.props.actions.closeModalAddOrganization}
                type="button"
                className="close"
              >
                <span>&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <form className="item-add-form d-flex flex-column">
                <label>
                  Name:
                  <input
                    type="text"
                    placeholder="Введите название"
                    className="form-control"
                    required
                    onChange={this.nameChange}
                  />
                </label>
                <label>
                  Address:
                  <input
                    type="text"
                    placeholder="Введите адрес"
                    className="form-control"
                    required
                    onChange={this.addressChange}
                  />
                </label>
                <label>
                  INN:
                  <input
                    type="text"
                    placeholder="Введите ИНН"
                    className="form-control"
                    required
                    onChange={this.innChange}
                  />
                </label>
              </form>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-primary"
                onClick={() => {
                  let Data = {
                    name: this.state.name,
                    address: this.state.address,
                    INN: this.state.INN
                  };
                  this.AddOrganization(Data);
                }}
              >
                Подтвердить
              </button>
              <button
                onClick={this.props.actions.closeModalAddOrganization}
                type="button"
                className="btn btn-secondary"
              >
                Закрыть
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(store: IStoreState): IStatePropsModalAddOrganization {
  return {
    isOpenOrg: store.isOpenOrg,
    organization: store.organization
  };
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatch {
  return {
    actions: new Actions(dispatch)
  };
}

const connectModalAddOrganization = connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalAddOrganization);
export { connectModalAddOrganization as ModalAddOrganization };
