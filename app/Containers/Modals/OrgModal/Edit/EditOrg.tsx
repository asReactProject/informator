import * as React from "react";
import { Actions } from "../../../../Actions/Actions";
import { IStoreState } from "../../../../Interfaces";
import { IOrganization } from "../../../../Actions/Models";
import { Dispatch } from "redux";
import { IActionType } from "../../../../common";
import {
  IDispatch,
  IStatePropsModalEditOrganization
} from "../../../../Interfaces";
import { connect } from "react-redux";
import "./EditOrg.less";

type IModalDelOrganization = IStatePropsModalEditOrganization & IDispatch & any;

class ModalEditOrganization extends React.Component<IModalDelOrganization> {
  state = {
    id: 0,
    name: "",
    address: "",
    INN: 0
  };

  nameChange = (e: any) => {
    this.setState({
      name: e.target.value
    });
  };

  addressChange = (e: any) => {
    this.setState({
      address: e.target.value
    });
  };

  innChange = (e: any) => {
    this.setState({
      INN: e.target.value
    });
  };

  EditOrganization = (data: IOrganization) => {
    this.props.actions.EditOrganization(data);
  };

  render() {
    const { isOpenEditOrg } = this.props;

    const { id, name, address, INN } = this.props.organization;

    if (!isOpenEditOrg) return null;
    return (
      <div className="modal">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Редактирование записи</h5>
              <button
                onClick={this.props.actions.closeModalEditOrganization}
                type="button"
                className="close"
              >
                <span>&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <form className="item-add-form d-flex flex-column">
                <label>
                  Name:
                  <input
                    type="text"
                    placeholder={name}
                    className="form-control"
                    required
                    onChange={this.nameChange}
                  />
                </label>
                <label>
                  Address:
                  <input
                    type="text"
                    placeholder={address}
                    className="form-control"
                    required
                    onChange={this.addressChange}
                  />
                </label>
                <label>
                  INN:
                  <input
                    type="text"
                    placeholder={INN}
                    className="form-control"
                    required
                    onChange={this.innChange}
                  />
                </label>
              </form>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-primary"
                onClick={() => {
                  let Data = {
                    id: id,
                    name: this.state.name,
                    address: this.state.address,
                    INN: this.state.INN
                  };
                  this.EditOrganization(Data);
                }}
              >
                Подтвердить
              </button>
              <button
                onClick={this.props.actions.closeModalEditOrganization}
                type="button"
                className="btn btn-secondary"
              >
                Закрыть
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(store: IStoreState): IStatePropsModalEditOrganization {
  return {
    isOpenEditOrg: store.isOpenEditOrg,
    organization: store.organization
  };
}

function mapDispatchToProps(dispatch: Dispatch<IActionType>): IDispatch {
  return {
    actions: new Actions(dispatch)
  };
}

const connectModalEditOrganization = connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalEditOrganization);
export { connectModalEditOrganization as ModalEditOrganization };
