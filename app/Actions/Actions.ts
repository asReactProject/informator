import { Dispatch } from "redux";
import { IActionType } from "../common";
import { ActionTypes, AsyncActionTypes } from "./Constants";
import { ILoginData, IOrganization } from "./Models";

export class Actions {
  constructor(private dispatch: Dispatch<IActionType>) {}
  onLogin = (loginData: ILoginData) => {
    this.dispatch({ type: `${ActionTypes.LOGIN}${AsyncActionTypes.BEGIN}` });
    const options = {
      method: "POST",
      body: JSON.stringify({ loginData }),
      headers: {
        "Content-Type": "application/json;charset=utf-8"
      }
    };
    fetch("http://127.0.0.1:3002/athorize", options).then(response => {
      if (response.status === 200) {
        this.dispatch({
          type: `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`,
          payload: loginData.name
        });
      } else {
        throw "error";
      }
    });
  };

  onLogout = () => {
    this.dispatch({ type: ActionTypes.LOGOUT });
    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json;charset=utf-8"
      }
    };
    fetch("http://127.0.0.1:3002/logout", options)
      .then(response => {
        if (response.status === 200) {
          this.dispatch({ type: ActionTypes.LOGOUT });
        } else {
          throw "error";
        }
      })
      .catch(() => {
        this.dispatch({ type: ActionTypes.LOGOUT });
      });
  };

  OrgFethData = (url: string) => {
    const options = {
      method: "GET",
      headers: {
        "Content-Type": "application/json;charset=utf-8"
      }
    };

    fetch(url, options)
      .then(response => {
        if (!response.ok) {
          console.log("ошибка");
          throw new Error(response.statusText);
        }
        return response;
      })
      .then(response => response.json())
      .then(organizations =>
        this.dispatch({
          type: `${ActionTypes.ORGANIZATIONS}${AsyncActionTypes.SUCCESS}`,
          organizations
        })
      );
  };

  DivFethData = (url: string) => {
    const options = {
      method: "GET",
      headers: {
        "Content-Type": "application/json;charset=utf-8"
      }
    };

    fetch(url, options)
      .then(response => {
        if (!response.ok) {
          console.log("ошибка");
          throw new Error(response.statusText);
        }
        return response;
      })
      .then(response => response.json())
      .then(divisions =>
        this.dispatch({
          type: `${ActionTypes.DIVISIONS}${AsyncActionTypes.SUCCESS}`,
          divisions
        })
      );
  };

  EpmlFethData = (url: string) => {
    const options = {
      method: "GET",
      headers: {
        "Content-Type": "application/json;charset=utf-8"
      }
    };

    fetch(url, options)
      .then(response => {
        if (!response.ok) {
          console.log("ошибка");
          throw new Error(response.statusText);
        }
        return response;
      })
      .then(response => response.json())
      .then(employees =>
        this.dispatch({
          type: `${ActionTypes.EMPLOYEES}${AsyncActionTypes.SUCCESS}`,
          employees
        })
      );
  };

  openModalAddOrganization = () => {
    this.dispatch({
      type: `${ActionTypes.OPEN_MODAL_ADD_ORGANIZATION}`,
      payload: {}
    });
  };

  closeModalAddOrganization = () => {
    this.dispatch({ type: `${ActionTypes.CLOSE_MODAL_ADD_ORGANIZATION}` });
  };

  AddOrganization = (data: IOrganization) => {
    console.log(data);
    const options = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json;charset=utf-8"
      }
    };
    fetch("http://127.0.0.1:3002/createOrganization", options).then(
      response => {
        if (response.status === 200) {
          console.log("Редактирование завершено");
        }
      }
    );
  };

  openModalEditOrganization = (
    id: number,
    name: string,
    address: string,
    INN: number
  ) => {
    const organization = {
      id,
      name,
      address,
      INN
    };
    this.dispatch({
      type: `${ActionTypes.OPEN_MODAL_EDIT_ORGANIZATION}`,
      payload: organization
    });
  };

  closeModalEditOrganization = () => {
    this.dispatch({ type: `${ActionTypes.CLOSE_MODAL_EDIT_ORGANIZATION}` });
  };

  EditOrganization = (data: IOrganization) => {
    console.log(data);
    const options = {
      method: "put",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json;charset=utf-8"
      }
    };
    fetch("http://127.0.0.1:3002/editOrganization", options).then(response => {
      if (response.status === 200) {
        console.log("Редактирование завершено");
      }
    });
  };
}
