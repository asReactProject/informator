import "bootstrap/dist/css/bootstrap.min.css";
import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { App } from "./App/App";
import { Store } from "./Store/Store";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
ReactDOM.render(
  <Provider store={Store}>
    <App />
  </Provider>,
  document.getElementById("app")
);
